# FILE: logger.php
## USAGE:
### Add the following to the top of the file you want to do logging in:
```php
include('path/logger.php');
```

### To log something:
```php
print_log($level, $message, [$data]);
```

- **level:**   *\[required]*  Can be integer or string (of debug..error, not case sensitive). 
- **message:** *\[required]*  The main data to be logged.
- **data:**    *\[optional]*  Will be logged as a bullet point after $message. If an array is passed, multiple bullets can be added. Sub-bullets are added by nesting arrays.

### Verbosity:
Set logging verbosity via the $LOG_LEVEL global. All messages up to and including the loglevel will 
be logged. Eg. If a loglevel of "ERROR" is set, only error messages will be logged. If "INFO" is set,
"INFO", "WARNING" and "ERROR" messages are logged. "DEBUG" (or 0) logs all messages.

### Date-Time stamp:
The date format can be set using the $LOG_DTFMT global (Date-Time-Format). For more info on date
formatting, see: http://php.net/manual/en/function.date.php


## EXAMPLE:
### LIVE:
https://ideone.com/b8yO5o


### COMMAND:
```php
print_log("DEBUG", "Test log message");
```

### OUTPUT:
```
20190119-024708[DEBUG]:  Test log message
```


### COMMAND:
```php
print_log("DEBUG", "Test log message", [0, 1, [2.1, 2.2], 3]);
```

### OUTPUT:
```
20190119-024708[DEBUG]:  Test log message
  - 0
  - 1
      - 2.1
      - 2.2
  - 3
```

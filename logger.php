<?php
    //-- Defaults
    $LOG_LEVEL = "ERROR";
    $LOG_DTFMT = "Ymd-His";
    $LOG_FILE = "/var/log/loki-logfile.log";
    
    
    //-- Private global
    $_LOGLEVELS = [
        'DEBUG',    /** Has extensive contextual information. They are mostly used for problem diagnosis.
                        Information on this Level are for Developers and not for the Users. **/
        'INFO',     /** Has some contextual information to help trace execution (at a coarse-grained level)
                        in a production environment. **/
        'WARNING',  /** A warning message indicates a potential problem in the system. The System is able
                        to handle the problem by themself or to proccede with this problem anyway. **/
        'ERROR'     /** [DEFAULT] An error message indicates a serious problem in the system. The problem
                        is usually non-recoverable and requires manual intervention. **/
    ];


    //--[ Private ]-------------------------------------------------------------------------------------------------
    function _get_loglevel_number($level) {
        global $_LOGLEVELS;

        switch(gettype($level)) {
            case "integer":
                //-- Make sure a relevant number is selected
                if (($level >= 0) && ($level < count($_LOGLEVELS))) {
                    return $level;
                }
                break;

            case "string":
                if (is_numeric($level)) {
                    //-- Pass as an integer
                    return _get_loglevel_number((int)$level);
                } else {
                    //-- Find the relevant array index
                    $number = array_search(strtoupper(trim($level)), $_LOGLEVELS);
                    if (is_numeric($number)) {
                        return $number;
                    }
                }
                break;

            default:
                break;
        }

        //-- Default
        print_log('DEBUG', "Unknown loglevel passed", "$level");
        return _get_loglevel_number("ERROR");
    }

    function _get_loglevel() {
        global $LOG_LEVEL;
        return _get_loglevel_number(((isset($LOG_LEVEL)) ? $LOG_LEVEL : "ERROR"));
    }

    function _sexy_array($prefix, $array) {
        $temp = "";
        foreach ($array as $item) {
            if (gettype($item) == "array") {
                $temp .= _sexy_array("    $prefix", $item);
            } else {
                $temp .= "$prefix$item\n";
            }
        }
        return $temp;
    }
    
    
    //--[ Public ]--------------------------------------------------------------------------------------------------
    function print_log($level, $message, $data=null) {
        global $_LOGLEVELS;
        global $LOG_FILE;
        global $LOG_DTFMT;
        
        $_level = _get_loglevel_number($level);
        if (_get_loglevel() <= $_level) {
            //-- Build log string
            $str_log = date($LOG_DTFMT)."[".$_LOGLEVELS[$_level]."]:  $message\n";
            if (!is_null($data)) {
                $str_log .= _sexy_array("  - ", (gettype($data) == "array") ? $data : [$data]);
            }
            
            //-- Do logging
            file_put_contents($LOG_FILE, $str_log, FILE_APPEND | LOCK_EX);
        }
    }
?>
